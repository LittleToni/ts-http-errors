# ts-http-errors

## Getting started

You can very easily reference all the exceptions contained within

#### Install

```sh
npm i ts-http-errors-response
```

#### Default Usage

```typescript
import { BadRequestResponse } from 'ts-http-errors-response';

throw new BadRequestResponse();
```

Expected object:

```json
{
  "statusCode": 400,
  "message": "Bad Request"
}
```

#### Usage with custom message:

```typescript
import { BadRequestResponse } from 'ts-http-errors-response';

throw new BadRequestResponse('Custom error message');
```

Expected object:

```json
{
  "statusCode": 400,
  "message": "Custom error message"
}
```

## API

|Code       |Name                                  |
|:----------|:-------------------------------------|
|400        |BadRequestResponse                    |
|401        |UnauthorizedResponse                  |
|402        |PaymentRequiredResponse               |
|403        |ForbiddenResponse                     |
|404        |NotFoundResponse                      |
|405        |MethodNotAllowedResponse              |
|406        |NotAcceptableResponse                 |
|407        |ProxyAuthenticationRequiredResponse   |
|408        |RequestTimeoutResponse                |
|409        |ConflictResponse                      |
|410        |GoneResponse                          |
|411        |LengthRequiredResponse                |
|412        |PreconditionFailedResponse            |
|413        |PayloadTooLargeResponse               |
|414        |URITooLongResponse                    |
|415        |UnsupportedMediaTypeResponse          |
|416        |RangeNotSatisfiableResponse           |
|417        |ExpectationFailedResponse             |
|418        |IAmATeapotResponse                    |
|419        |AuthenticationTimeoutResponse         |
|421        |MisdirectedRequestResponse            |
|422        |UnprocessableEntityResponse           |
|423        |LockedResponse                        |
|424        |FailedDependencyResponse              |
|426        |UpgradeRequiredResponse               |
|428        |PreconditionRequiredResponse          |
|429        |TooManyRequestsResponse               |
|431        |RequestHeaderFieldsTooLargeResponse   |
|449        |RetryWithResponse                     |
|451        |UnavailableForLegalReasonsResponse    |
|499        |ClientClosedRequestResponse           |
|500        |InternalServerResponse                |
|501        |NotImplementedResponse                |
|502        |BadGatewayResponse                    |
|503        |ServiceUnavailableResponse            |
|504        |GatewayTimeoutResponse                |
|505        |HTTPVersionNotSupportedResponse       |
|506        |VariantAlsoNegotiatesResponse         |
|507        |InsufficientStorageResponse           |
|508        |LoopDetectedResponse                  |
|509        |BandwidthLimitExceededResponse        |
|510        |NotExtendedResponse                   |
|511        |NetworkAuthenticationRequiredResponse |
|520        |UnknownResponse                       |
|521        |WebServerIsDownResponse               |
|522        |ConnectionTimedOutResponse            |
|523        |OriginIsUnreachableResponse           |
|524        |ATimeoutOccurredResponse              |
|525        |SSLHandshakeFailedResponse            |
|526        |InvalidSSLCertificateResponse         |
