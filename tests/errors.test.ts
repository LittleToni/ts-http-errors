import {
    BadRequestResponse,
    UnauthorizedResponse,
    PaymentRequiredResponse,
    ForbiddenResponse,
    NotFoundResponse,
    MethodNotAllowedResponse,
    NotAcceptableResponse,
    ProxyAuthenticationRequiredResponse,
    RequestTimeoutResponse,
    ConflictResponse,
    GoneResponse,
    LengthRequiredResponse,
    PreconditionFailedResponse,
    PayloadTooLargeResponse,
    URITooLongResponse,
    UnsupportedMediaTypeResponse,
    RangeNotSatisfiableResponse,
    ExpectationFailedResponse,
    IAmATeapotResponse,
    AuthenticationTimeoutResponse,
    MisdirectedRequestResponse,
    UnprocessableEntityResponse,
    LockedResponse,
    FailedDependencyResponse,
    UpgradeRequiredResponse,
    TooManyRequestsResponse,
    RequestHeaderFieldsTooLargeResponse,
    RetryWithResponse,
    UnavailableForLegalReasonsResponse,
    PreconditionRequiredResponse,
    ClientClosedRequestResponse,
    InternalServerResponse,
    NotImplementedResponse,
    BadGatewayResponse,
    ServiceUnavailableResponse,
    GatewayTimeoutResponse,
    HTTPVersionNotSupportedResponse,
    VariantAlsoNegotiatesResponse,
    InsufficientStorageResponse,
    LoopDetectedResponse,
    BandwidthLimitExceededResponse,
    NotExtendedResponse,
    NetworkAuthenticationRequiredResponse,
    UnknownResponse,
    WebServerIsDownResponse,
    ConnectionTimedOutResponse,
    OriginIsUnreachableResponse,
    ATimeoutOccurredResponse,
    SSLHandshakeFailedResponse,
    InvalidSSLCertificateResponse
} from '../src';

describe('test http errors', () => {
    let customErrorMessage: string;

    beforeAll(() => {
        customErrorMessage = 'custom error message';
    });

    it('BadRequestResponse', () => {
        expect(() => {throw new BadRequestResponse()}).toThrow(BadRequestResponse);
        expect(() => {throw new BadRequestResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('UnauthorizedResponse', () => {
        expect(() => {throw new UnauthorizedResponse()}).toThrow(UnauthorizedResponse);
        expect(() => {throw new UnauthorizedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('PaymentRequiredResponse', () => {
        expect(() => {throw new PaymentRequiredResponse()}).toThrow(PaymentRequiredResponse);
        expect(() => {throw new PaymentRequiredResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('ForbiddenResponse', () => {
        expect(() => {throw new ForbiddenResponse()}).toThrow(ForbiddenResponse);
        expect(() => {throw new ForbiddenResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('NotFoundResponse', () => {
        expect(() => {throw new NotFoundResponse()}).toThrow(NotFoundResponse);
        expect(() => {throw new NotFoundResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('MethodNotAllowedResponse', () => {
        expect(() => {throw new MethodNotAllowedResponse()}).toThrow(MethodNotAllowedResponse);
        expect(() => {throw new MethodNotAllowedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('NotAcceptableResponse', () => {
        expect(() => {throw new NotAcceptableResponse()}).toThrow(NotAcceptableResponse);
        expect(() => {throw new NotAcceptableResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('ProxyAuthenticationRequiredResponse', () => {
        expect(() => {throw new ProxyAuthenticationRequiredResponse()}).toThrow(ProxyAuthenticationRequiredResponse);
        expect(() => {throw new ProxyAuthenticationRequiredResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('RequestTimeoutResponse', () => {
        expect(() => {throw new RequestTimeoutResponse()}).toThrow(RequestTimeoutResponse);
        expect(() => {throw new RequestTimeoutResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('ConflictResponse', () => {
        expect(() => {throw new ConflictResponse()}).toThrow(ConflictResponse);
        expect(() => {throw new ConflictResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('GoneResponse', () => {
        expect(() => {throw new GoneResponse()}).toThrow(GoneResponse);
        expect(() => {throw new GoneResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('LengthRequiredResponse', () => {
        expect(() => {throw new LengthRequiredResponse()}).toThrow(LengthRequiredResponse);
        expect(() => {throw new LengthRequiredResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('PreconditionFailedResponse', () => {
        expect(() => {throw new PreconditionFailedResponse()}).toThrow(PreconditionFailedResponse);
        expect(() => {throw new PreconditionFailedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('PayloadTooLargeResponse', () => {
        expect(() => {throw new PayloadTooLargeResponse()}).toThrow(PayloadTooLargeResponse);
        expect(() => {throw new PayloadTooLargeResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('URITooLongResponse', () => {
        expect(() => {throw new URITooLongResponse()}).toThrow(URITooLongResponse);
        expect(() => {throw new URITooLongResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('UnsupportedMediaTypeResponse', () => {
        expect(() => {throw new UnsupportedMediaTypeResponse()}).toThrow(UnsupportedMediaTypeResponse);
        expect(() => {throw new UnsupportedMediaTypeResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('RangeNotSatisfiableResponse', () => {
        expect(() => {throw new RangeNotSatisfiableResponse()}).toThrow(RangeNotSatisfiableResponse);
        expect(() => {throw new RangeNotSatisfiableResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('ExpectationFailedResponse', () => {
        expect(() => {throw new ExpectationFailedResponse()}).toThrow(ExpectationFailedResponse);
        expect(() => {throw new ExpectationFailedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('IAmATeapotResponse', () => {
        expect(() => {throw new IAmATeapotResponse()}).toThrow(IAmATeapotResponse);
        expect(() => {throw new IAmATeapotResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('AuthenticationTimeoutResponse', () => {
        expect(() => {throw new AuthenticationTimeoutResponse()}).toThrow(AuthenticationTimeoutResponse);
        expect(() => {throw new AuthenticationTimeoutResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('MisdirectedRequestResponse', () => {
        expect(() => {throw new MisdirectedRequestResponse()}).toThrow(MisdirectedRequestResponse);
        expect(() => {throw new MisdirectedRequestResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('UnprocessableEntityResponse', () => {
        expect(() => {throw new UnprocessableEntityResponse()}).toThrow(UnprocessableEntityResponse);
        expect(() => {throw new UnprocessableEntityResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('LockedResponse', () => {
        expect(() => {throw new LockedResponse()}).toThrow(LockedResponse);
        expect(() => {throw new LockedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('FailedDependencyResponse', () => {
        expect(() => {throw new FailedDependencyResponse()}).toThrow(FailedDependencyResponse);
        expect(() => {throw new FailedDependencyResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('UpgradeRequiredResponse', () => {
        expect(() => {throw new UpgradeRequiredResponse()}).toThrow(UpgradeRequiredResponse);
        expect(() => {throw new UpgradeRequiredResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('PreconditionRequiredResponse', () => {
        expect(() => {throw new PreconditionRequiredResponse()}).toThrow(PreconditionRequiredResponse);
        expect(() => {throw new PreconditionRequiredResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('TooManyRequestsResponse', () => {
        expect(() => {throw new TooManyRequestsResponse()}).toThrow(TooManyRequestsResponse);
        expect(() => {throw new TooManyRequestsResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('RequestHeaderFieldsTooLargeResponse', () => {
        expect(() => {throw new RequestHeaderFieldsTooLargeResponse()}).toThrow(RequestHeaderFieldsTooLargeResponse);
        expect(() => {throw new RequestHeaderFieldsTooLargeResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('RetryWithResponse', () => {
        expect(() => {throw new RetryWithResponse()}).toThrow(RetryWithResponse);
        expect(() => {throw new RetryWithResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('UnavailableForLegalReasonsResponse', () => {
        expect(() => {throw new UnavailableForLegalReasonsResponse()}).toThrow(UnavailableForLegalReasonsResponse);
        expect(() => {throw new UnavailableForLegalReasonsResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('ClientClosedRequestResponse', () => {
        expect(() => {throw new ClientClosedRequestResponse()}).toThrow(ClientClosedRequestResponse);
        expect(() => {throw new ClientClosedRequestResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('InternalServerResponse', () => {
        expect(() => {throw new InternalServerResponse()}).toThrow(InternalServerResponse);
        expect(() => {throw new InternalServerResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('NotImplementedResponse', () => {
        expect(() => {throw new NotImplementedResponse()}).toThrow(NotImplementedResponse);
        expect(() => {throw new NotImplementedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('BadGatewayResponse', () => {
        expect(() => {throw new BadGatewayResponse()}).toThrow(BadGatewayResponse);
        expect(() => {throw new BadGatewayResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('ServiceUnavailableResponse', () => {
        expect(() => {throw new ServiceUnavailableResponse()}).toThrow(ServiceUnavailableResponse);
        expect(() => {throw new ServiceUnavailableResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('GatewayTimeoutResponse', () => {
        expect(() => {throw new GatewayTimeoutResponse()}).toThrow(GatewayTimeoutResponse);
        expect(() => {throw new GatewayTimeoutResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('HTTPVersionNotSupportedResponse', () => {
        expect(() => {throw new HTTPVersionNotSupportedResponse()}).toThrow(HTTPVersionNotSupportedResponse);
        expect(() => {throw new HTTPVersionNotSupportedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('VariantAlsoNegotiatesResponse', () => {
        expect(() => {throw new VariantAlsoNegotiatesResponse()}).toThrow(VariantAlsoNegotiatesResponse);
        expect(() => {throw new VariantAlsoNegotiatesResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('InsufficientStorageResponse', () => {
        expect(() => {throw new InsufficientStorageResponse()}).toThrow(InsufficientStorageResponse);
        expect(() => {throw new InsufficientStorageResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('LoopDetectedResponse', () => {
        expect(() => {throw new LoopDetectedResponse()}).toThrow(LoopDetectedResponse);
        expect(() => {throw new LoopDetectedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('BandwidthLimitExceededResponse', () => {
        expect(() => {throw new BandwidthLimitExceededResponse()}).toThrow(BandwidthLimitExceededResponse);
        expect(() => {throw new BandwidthLimitExceededResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('NotExtendedResponse', () => {
        expect(() => {throw new NotExtendedResponse()}).toThrow(NotExtendedResponse);
        expect(() => {throw new NotExtendedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('NetworkAuthenticationRequiredResponse', () => {
        expect(() => {throw new NetworkAuthenticationRequiredResponse()}).toThrow(NetworkAuthenticationRequiredResponse);
        expect(() => {throw new NetworkAuthenticationRequiredResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('UnknownResponse', () => {
        expect(() => {throw new UnknownResponse()}).toThrow(UnknownResponse);
        expect(() => {throw new UnknownResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('WebServerIsDownResponse', () => {
        expect(() => {throw new WebServerIsDownResponse()}).toThrow(WebServerIsDownResponse);
        expect(() => {throw new WebServerIsDownResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('ConnectionTimedOutResponse', () => {
        expect(() => {throw new ConnectionTimedOutResponse()}).toThrow(ConnectionTimedOutResponse);
        expect(() => {throw new ConnectionTimedOutResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('OriginIsUnreachableResponse', () => {
        expect(() => {throw new OriginIsUnreachableResponse()}).toThrow(OriginIsUnreachableResponse);
        expect(() => {throw new OriginIsUnreachableResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('ATimeoutOccurredResponse', () => {
        expect(() => {throw new ATimeoutOccurredResponse()}).toThrow(ATimeoutOccurredResponse);
        expect(() => {throw new ATimeoutOccurredResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('SSLHandshakeFailedResponse', () => {
        expect(() => {throw new SSLHandshakeFailedResponse()}).toThrow(SSLHandshakeFailedResponse);
        expect(() => {throw new SSLHandshakeFailedResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });

    it('InvalidSSLCertificateResponse', () => {
        expect(() => {throw new InvalidSSLCertificateResponse()}).toThrow(InvalidSSLCertificateResponse);
        expect(() => {throw new InvalidSSLCertificateResponse(customErrorMessage)}).toThrow(customErrorMessage);
    });
})