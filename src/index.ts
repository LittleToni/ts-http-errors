/**
 * Base class
 * 
 * https://www.typescriptlang.org/docs/handbook/2/classes.html#inheriting-built-in-types
 * As a recommendation, you can manually adjust the prototype immediately after any super(...) calls.
 * However, any subclass of MsgError will have to manually set the prototype as well.
 * For runtimes that don’t support Object.setPrototypeOf, you may instead be able to use __proto__.
 */
export abstract class HttpError extends Error {
    public statusCode?: number;

    constructor(name: string, public message: string) {
        super(message);
        this.name = name;
    }
}

/**
 * Class BadRequestResponse
 * Response with { statusCode: 400, message: "Bad Request" }
 * 
 * Examples:
 * 
 * throw new BadRequestResponse();
 * throw new BadRequestResponse('My Error Message');
 */
export class BadRequestResponse extends HttpError {
    constructor(message?: string) {
        super('BadRequestResponse', message || 'Bad Request');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 400;
    }
}

/**
 * Class UnauthorizedResponse
 * Response with { statusCode: 401, message: "Unauthorized" }
 *
 * Examples:
 *
 * throw new UnauthorizedResponse();
 * throw new UnauthorizedResponse('My Error Message');
 */
export class UnauthorizedResponse extends HttpError {
    constructor(message?: string) {
        super('UnauthorizedResponse', message || 'Unauthorized');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 401;
    }
}

/**
 * Class PaymentRequiredResponse
 * Response with { statusCode: 402, message: "Payment Required" }
 *
 * Examples:
 *
 * throw new PaymentRequiredResponse();
 * throw new PaymentRequiredResponse('My Error Message');
 */
export class PaymentRequiredResponse extends HttpError {
    constructor(message?: string) {
        super('PaymentRequiredResponse', message || 'Payment Required');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 402;
    }
}

/**
 * Class ForbiddenResponse
 * Response with { statusCode: 403, message: "Forbidden" }
 *
 * Examples:
 *
 * throw new ForbiddenResponse();
 * throw new ForbiddenResponse('My Error Message');
 */
export class ForbiddenResponse extends HttpError {
    constructor(message?: string) {
        super('ForbiddenResponse', message || 'Forbidden');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 403;
    }
}

/**
 * Class NotFoundResponse
 * Response with { statusCode: 404, message: "Not Found" }
 *
 * Examples:
 *
 * throw new NotFoundResponse();
 * throw new NotFoundResponse('My Error Message');
 */
export class NotFoundResponse extends HttpError {
    constructor(message?: string) {
        super('NotFoundResponse', message || 'Not Found');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 404;
    }
}

/**
 * Class MethodNotAllowedResponse
 * Response with { statusCode: 405, message: "Method Not Allowed" }
 *
 * Examples:
 *
 * throw new MethodNotAllowedResponse();
 * throw new MethodNotAllowedResponse('My Error Message');
 */
export class MethodNotAllowedResponse extends HttpError {
    constructor(message?: string) {
        super('MethodNotAllowedResponse', message || 'Method Not Allowed');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 405;
    }
}

/**
 * Class NotAcceptableResponse
 * Response with { statusCode: 406, message: "Not Acceptable" }
 *
 * Examples:
 *
 * throw new NotAcceptableResponse();
 * throw new NotAcceptableResponse('My Error Message');
 */
export class NotAcceptableResponse extends HttpError {
    constructor(message?: string) {
        super('NotAcceptableResponse', message || 'Not Acceptable');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 406;
    }
}

/**
 * Class ProxyAuthenticationRequiredResponse
 * Response with { statusCode: 407, message: "Proxy Authentication Required" }
 *
 * Examples:
 *
 * throw new ProxyAuthenticationRequiredResponse();
 * throw new ProxyAuthenticationRequiredResponse('My Error Message');
 */
export class ProxyAuthenticationRequiredResponse extends HttpError {
    constructor(message?: string) {
        super('ProxyAuthenticationRequiredResponse', message || 'Proxy Authentication Required');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 407;
    }
}

/**
 * Class RequestTimeoutResponse
 * Response with { statusCode: 408, message: "Request Timeout" }
 *
 * Examples:
 *
 * throw new RequestTimeoutResponse();
 * throw new RequestTimeoutResponse('My Error Message');
 */
export class RequestTimeoutResponse extends HttpError {
    constructor(message?: string) {
        super('RequestTimeoutResponse', message || 'Request Timeout');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 408;
    }
}

/**
 * Class ConflictResponse
 * Response with { statusCode: 409, message: "Conflict" }
 *
 * Examples:
 *
 * throw new ConflictResponse();
 * throw new ConflictResponse('My Error Message');
 */
export class ConflictResponse extends HttpError {
    constructor(message?: string) {
        super('ConflictResponse', message || 'Conflict');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 409;
    }
}

/**
 * Class GoneResponse
 * Response with { statusCode: 410, message: "Gone" }
 *
 * Examples:
 *
 * throw new GoneResponse();
 * throw new GoneResponse('My Error Message');
 */
export class GoneResponse extends HttpError {
    constructor(message?: string) {
        super('GoneResponse', message || 'Gone');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 410;
    }
}

/**
 * Class LengthRequiredResponse
 * Response with { statusCode: 411, message: "Length Required" }
 *
 * Examples:
 *
 * throw new LengthRequiredResponse();
 * throw new LengthRequiredResponse('My Error Message');
 */
export class LengthRequiredResponse extends HttpError {
    constructor(message?: string) {
        super('LengthRequiredResponse', message || 'Length Required');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 411;
    }
}

/**
 * Class PreconditionFailedResponse
 * Response with { statusCode: 412, message: "Precondition Failed" }
 *
 * Examples:
 *
 * throw new PreconditionFailedResponse();
 * throw new PreconditionFailedResponse('My Error Message');
 */
export class PreconditionFailedResponse extends HttpError {
    constructor(message?: string) {
        super('PreconditionFailedResponse', message || 'Precondition Failed');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 412;
    }
}

/**
 * Class PayloadTooLargeResponse
 * Response with { statusCode: 413, message: "Payload Too Large" }
 *
 * Examples:
 *
 * throw new PayloadTooLargeResponse();
 * throw new PayloadTooLargeResponse('My Error Message');
 */
export class PayloadTooLargeResponse extends HttpError {
    constructor(message?: string) {
        super('PayloadTooLargeResponse', message || 'Payload Too Large');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 413;
    }
}

/**
 * Class URITooLongResponse
 * Response with { statusCode: 414, message: "URI Too Long" }
 *
 * Examples:
 *
 * throw new URITooLongResponse();
 * throw new URITooLongResponse('My Error Message');
 */
export class URITooLongResponse extends HttpError {
    constructor(message?: string) {
        super('URITooLongResponse', message || 'URI Too Long');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 414;
    }
}

/**
 * Class UnsupportedMediaTypeResponse
 * Response with { statusCode: 415, message: "Unsupported Media Type" }
 *
 * Examples:
 *
 * throw new UnsupportedMediaTypeResponse();
 * throw new UnsupportedMediaTypeResponse('My Error Message');
 */
export class UnsupportedMediaTypeResponse extends HttpError {
    constructor(message?: string) {
        super('UnsupportedMediaTypeResponse', message || 'Unsupported Media Type');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 415;
    }
}

/**
 * Class RangeNotSatisfiableResponse
 * Response with { statusCode: 416, message: "Range Not Satisfiable" }
 *
 * Examples:
 *
 * throw new RangeNotSatisfiableResponse();
 * throw new RangeNotSatisfiableResponse('My Error Message');
 */
export class RangeNotSatisfiableResponse extends HttpError {
    constructor(message?: string) {
        super('RangeNotSatisfiableResponse', message || 'Range Not Satisfiable');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 416;
    }
}

/**
 * Class ExpectationFailedResponse
 * Response with { statusCode: 417, message: "Expectation Failed" }
 *
 * Examples:
 *
 * throw new ExpectationFailedResponse();
 * throw new ExpectationFailedResponse('My Error Message');
 */
export class ExpectationFailedResponse extends HttpError {
    constructor(message?: string) {
        super('ExpectationFailedResponse', message || 'Expectation Failed');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 417;
    }
}

/**
 * Class IAmATeapotResponse
 * Response with { statusCode: 418, message: "I Am A Teapot" }
 *
 * Examples:
 *
 * throw new IAmATeapotResponse();
 * throw new IAmATeapotResponse('My Error Message');
 */
export class IAmATeapotResponse extends HttpError {
    constructor(message?: string) {
        super('IAmATeapotResponse', message || 'I Am A Teapot');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 418;
    }
}

/**
 * Class AuthenticationTimeoutResponse
 * Response with { statusCode: 419, message: "Authentication Timeout" }
 *
 * Examples:
 *
 * throw new AuthenticationTimeoutResponse();
 * throw new AuthenticationTimeoutResponse('My Error Message');
 */
export class AuthenticationTimeoutResponse extends HttpError {
    constructor(message?: string) {
        super('AuthenticationTimeoutResponse', message || 'Authentication Timeout');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 419;
    }
}

/**
 * Class MisdirectedRequestResponse
 * Response with { statusCode: 421, message: "Misdirected Request" }
 *
 * Examples:
 *
 * throw new MisdirectedRequestResponse();
 * throw new MisdirectedRequestResponse('My Error Message');
 */
export class MisdirectedRequestResponse extends HttpError {
    constructor(message?: string) {
        super('MisdirectedRequestResponse', message || 'Misdirected Request');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 421;
    }
}

/**
 * Class UnprocessableEntityResponse
 * Response with { statusCode: 422, message: "Unprocessable Entity" }
 * 
 * Examples:
 * 
 * throw new UnprocessableEntityResponse();
 * throw new UnprocessableEntityResponse('My Error Message');
 */
export class UnprocessableEntityResponse extends HttpError {
    constructor(message?: string) {
        super('UnprocessableEntityResponse', message || 'Unprocessable Entity');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 422;
    }
}

/**
 * Class LockedResponse
 * Response with { statusCode: 423, message: "Locked" }
 *
 * Examples:
 *
 * throw new LockedResponse();
 * throw new LockedResponse('My Error Message');
 */
export class LockedResponse extends HttpError {
    constructor(message?: string) {
        super('LockedResponse', message || 'Locked');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 423;
    }
}

/**
 * Class FailedDependencyResponse
 * Response with { statusCode: 424, message: "Failed Dependency" }
 *
 * Examples:
 *
 * throw new FailedDependencyResponse();
 * throw new FailedDependencyResponse('My Error Message');
 */
export class FailedDependencyResponse extends HttpError {
    constructor(message?: string) {
        super('FailedDependencyResponse', message || 'Failed Dependency');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 424;
    }
}

/**
 * Class UpgradeRequiredResponse
 * Response with { statusCode: 426, message: "Upgrade Required" }
 *
 * Examples:
 *
 * throw new UpgradeRequiredResponse();
 * throw new UpgradeRequiredResponse('My Error Message');
 */
export class UpgradeRequiredResponse extends HttpError {
    constructor(message?: string) {
        super('UpgradeRequiredResponse', message || 'Upgrade Required');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 426;
    }
}

/**
 * Class PreconditionRequiredResponse
 * Response with { statusCode: 428, message: "Precondition Required" }
 *
 * Examples:
 *
 * throw new PreconditionRequiredResponse();
 * throw new PreconditionRequiredResponse('My Error Message');
 */
export class PreconditionRequiredResponse extends HttpError {
    constructor(message?: string) {
        super('PreconditionRequiredResponse', message || 'Precondition Required');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 428;
    }
}

/**
 * Class TooManyRequestsResponse
 * Response with { statusCode: 429, message: "Too Many Requests" }
 *
 * Examples:
 *
 * throw new TooManyRequestsResponse();
 * throw new TooManyRequestsResponse('My Error Message');
 */
export class TooManyRequestsResponse extends HttpError {
    constructor(message?: string) {
        super('TooManyRequestsResponse', message || 'Too Many Requests');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 429;
    }
}

/**
 * Class RequestHeaderFieldsTooLargeResponse
 * Response with { statusCode: 431, message: "Request Header Fields Too Large" }
 *
 * Examples:
 *
 * throw new RequestHeaderFieldsTooLargeResponse();
 * throw new RequestHeaderFieldsTooLargeResponse('My Error Message');
 */
export class RequestHeaderFieldsTooLargeResponse extends HttpError {
    constructor(message?: string) {
        super('RequestHeaderFieldsTooLargeResponse', message || 'Request Header Fields Too Large');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 431;
    }
}

/**
 * Class RetryWithResponse
 * Response with { statusCode: 449, message: "Retry With" }
 *
 * Examples:
 *
 * throw new RetryWithResponse();
 * throw new RetryWithResponse('My Error Message');
 */
export class RetryWithResponse extends HttpError {
    constructor(message?: string) {
        super('RetryWithResponse', message || 'Retry With');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 449;
    }
}

/**
 * Class UnavailableForLegalReasonsResponse
 * Response with { statusCode: 451, message: "Unavailable For Legal Reasons" }
 *
 * Examples:
 *
 * throw new UnavailableForLegalReasonsResponse();
 * throw new UnavailableForLegalReasonsResponse('My Error Message');
 */
export class UnavailableForLegalReasonsResponse extends HttpError {
    constructor(message?: string) {
        super('UnavailableForLegalReasonsResponse', message || 'Unavailable For Legal Reasons');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 451;
    }
}

/**
 * Class ClientClosedRequestResponse
 * Response with { statusCode: 499, message: "Client Closed Request" }
 *
 * Examples:
 *
 * throw new ClientClosedRequestResponse();
 * throw new ClientClosedRequestResponse('My Error Message');
 */
export class ClientClosedRequestResponse extends HttpError {
    constructor(message?: string) {
        super('ClientClosedRequestResponse', message || 'Client Closed Request');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 499;
    }
}

/**
 * Class InternalServerResponse
 * Response with { statusCode: 500, message: "Internal Server" }
 *
 * Examples:
 *
 * throw new InternalServerResponse();
 * throw new InternalServerResponse('My Error Message');
 */
export class InternalServerResponse extends HttpError {
    constructor(message?: string) {
        super('InternalServerResponse', message || 'Internal Server');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 500;
    }
}

/**
 * Class NotImplementedResponse
 * Response with { statusCode: 501, message: "Not Implemented" }
 *
 * Examples:
 *
 * throw new NotImplementedResponse();
 * throw new NotImplementedResponse('My Error Message');
 */
export class NotImplementedResponse extends HttpError {
    constructor(message?: string) {
        super('NotImplementedResponse', message || 'Not Implemented');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 501;
    }
}

/**
 * Class BadGatewayResponse
 * Response with { statusCode: 502, message: "Bad Gateway" }
 *
 * Examples:
 *
 * throw new BadGatewayResponse();
 * throw new BadGatewayResponse('My Error Message');
 */
export class BadGatewayResponse extends HttpError {
    constructor(message?: string) {
        super('BadGatewayResponse', message || 'Bad Gateway');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 502;
    }
}

/**
 * Class ServiceUnavailableResponse
 * Response with { statusCode: 503, message: "Service Unavailable" }
 *
 * Examples:
 *
 * throw new ServiceUnavailableResponse();
 * throw new ServiceUnavailableResponse('My Error Message');
 */
export class ServiceUnavailableResponse extends HttpError {
    constructor(message?: string) {
        super('ServiceUnavailableResponse', message || 'Service Unavailable');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 503;
    }
}

/**
 * Class GatewayTimeoutResponse
 * Response with { statusCode: 504, message: "Gateway Timeout" }
 *
 * Examples:
 *
 * throw new GatewayTimeoutResponse();
 * throw new GatewayTimeoutResponse('My Error Message');
 */
export class GatewayTimeoutResponse extends HttpError {
    constructor(message?: string) {
        super('GatewayTimeoutResponse', message || 'Gateway Timeout');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 504;
    }
}

/**
 * Class HTTPVersionNotSupportedResponse
 * Response with { statusCode: 505, message: "HTTP Version Not Supported" }
 *
 * Examples:
 *
 * throw new HTTPVersionNotSupportedResponse();
 * throw new HTTPVersionNotSupportedResponse('My Error Message');
 */
export class HTTPVersionNotSupportedResponse extends HttpError {
    constructor(message?: string) {
        super('HTTPVersionNotSupportedResponse', message || 'HTTP Version Not Supported');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 505;
    }
}

/**
 * Class VariantAlsoNegotiatesResponse
 * Response with { statusCode: 506, message: "Variant Also Negotiates" }
 *
 * Examples:
 *
 * throw new VariantAlsoNegotiatesResponse();
 * throw new VariantAlsoNegotiatesResponse('My Error Message');
 */
export class VariantAlsoNegotiatesResponse extends HttpError {
    constructor(message?: string) {
        super('VariantAlsoNegotiatesResponse', message || 'Variant Also Negotiates');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 506;
    }
}

/**
 * Class InsufficientStorageResponse
 * Response with { statusCode: 507, message: "Insufficient Storage" }
 *
 * Examples:
 *
 * throw new InsufficientStorageResponse();
 * throw new InsufficientStorageResponse('My Error Message');
 */
export class InsufficientStorageResponse extends HttpError {
    constructor(message?: string) {
        super('InsufficientStorageResponse', message || 'Insufficient Storage');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 507;
    }
}

/**
 * Class LoopDetectedResponse
 * Response with { statusCode: 508, message: "Loop Detected" }
 *
 * Examples:
 *
 * throw new LoopDetectedResponse();
 * throw new LoopDetectedResponse('My Error Message');
 */
export class LoopDetectedResponse extends HttpError {
    constructor(message?: string) {
        super('LoopDetectedResponse', message || 'Loop Detected');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 508;
    }
}

/**
 * Class BandwidthLimitExceededResponse
 * Response with { statusCode: 509, message: "Band width Limit Exceeded" }
 *
 * Examples:
 *
 * throw new BandwidthLimitExceededResponse();
 * throw new BandwidthLimitExceededResponse('My Error Message');
 */
export class BandwidthLimitExceededResponse extends HttpError {
    constructor(message?: string) {
        super('BandwidthLimitExceededResponse', message || 'Band width Limit Exceeded');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 509;
    }
}

/**
 * Class NotExtendedResponse
 * Response with { statusCode: 510, message: "Not Extended" }
 *
 * Examples:
 *
 * throw new NotExtendedResponse();
 * throw new NotExtendedResponse('My Error Message');
 */
export class NotExtendedResponse extends HttpError {
    constructor(message?: string) {
        super('NotExtendedResponse', message || 'Not Extended');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 510;
    }
}

/**
 * Class NetworkAuthenticationRequiredResponse
 * Response with { statusCode: 511, message: "Network Authentication Required" }
 *
 * Examples:
 *
 * throw new NetworkAuthenticationRequiredResponse();
 * throw new NetworkAuthenticationRequiredResponse('My Error Message');
 */
export class NetworkAuthenticationRequiredResponse extends HttpError {
    constructor(message?: string) {
        super('NetworkAuthenticationRequiredResponse', message || 'NetworkAuthenticationRequiredResponse');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 511;
    }
}

/**
 * Class UnknownResponse
 * Response with { statusCode: 520, message: "Unknown" }
 *
 * Examples:
 *
 * throw new UnknownResponse();
 * throw new UnknownResponse('My Error Message');
 */
export class UnknownResponse extends HttpError {
    constructor(message?: string) {
        super('UnknownResponse', message || 'Unknown');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 520;
    }
}

/**
 * Class WebServerIsDownResponse
 * Response with { statusCode: 521, message: "Web Server Is Down" }
 *
 * Examples:
 *
 * throw new WebServerIsDownResponse();
 * throw new WebServerIsDownResponse('My Error Message');
 */
export class WebServerIsDownResponse extends HttpError {
    constructor(message?: string) {
        super('WebServerIsDownResponse', message || 'Web Server Is Down');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 521;
    }
}

/**
 * Class ConnectionTimedOutResponse
 * Response with { statusCode: 522, message: "Connection Timed Out" }
 *
 * Examples:
 *
 * throw new ConnectionTimedOutResponse();
 * throw new ConnectionTimedOutResponse('My Error Message');
 */
export class ConnectionTimedOutResponse extends HttpError {
    constructor(message?: string) {
        super('ConnectionTimedOutResponse', message || 'Connection Timed Out');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 522;
    }
}

/**
 * Class OriginIsUnreachableResponse
 * Response with { statusCode: 523, message: "Origin Is Unreachable" }
 *
 * Examples:
 *
 * throw new OriginIsUnreachableResponse();
 * throw new OriginIsUnreachableResponse('My Error Message');
 */
export class OriginIsUnreachableResponse extends HttpError {
    constructor(message?: string) {
        super('OriginIsUnreachableResponse', message || 'Origin Is Unreachable');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 523;
    }
}

/**
 * Class ATimeoutOccurredResponse
 * Response with { statusCode: 524, message: "A Timeout Occurred" }
 *
 * Examples:
 *
 * throw new ATimeoutOccurredResponse();
 * throw new ATimeoutOccurredResponse('My Error Message');
 */
export class ATimeoutOccurredResponse extends HttpError {
    constructor(message?: string) {
        super('ATimeoutOccurredResponse', message || 'A Timeout Occurred');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 524;
    }
}

/**
 * Class SSLHandshakeFailedResponse
 * Response with { statusCode: 525, message: "SSL Handshake Failed" }
 *
 * Examples:
 *
 * throw new SSLHandshakeFailedResponse();
 * throw new SSLHandshakeFailedResponse('My Error Message');
 */
export class SSLHandshakeFailedResponse extends HttpError {
    constructor(message?: string) {
        super('SSLHandshakeFailedResponse', message || 'SSL Handshake Failed');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 525;
    }
}

/**
 * Class InvalidSSLCertificateResponse
 * Response with { statusCode: 526, message: "Invalid SSL Certificate" }
 *
 * Examples:
 *
 * throw new InvalidSSLCertificateResponse();
 * throw new InvalidSSLCertificateResponse('My Error Message');
 */
export class InvalidSSLCertificateResponse extends HttpError {
    constructor(message?: string) {
        super('InvalidSSLCertificateResponse', message || 'Invalid SSL Certificate');
        Object.setPrototypeOf(this, new.target.prototype);
        this.statusCode = 526;
    }
}
